extern crate irc;
extern crate rand;

use irc::client::prelude::*;
use rand::Rng;

fn main() {
    println!("Hello, world!");
    
    let config = Config {
        nickname: Some("AynRandBot".to_owned()),
        server: Some("irc.quakenet.org".to_owned()),
        //channels: Some(vec!["#xonotic.butt".to_owned()]),
        channels: Some(vec!["#xonotic.butt".to_owned()]),
        ..Config::default()
    };
    
    let mut reactor = IrcReactor::new().unwrap();
    let client = reactor.prepare_client_and_connect(&config).unwrap();
    client.identify().unwrap();
    
    reactor.register_client_with_handler(client, |client, message| {
        println!("{}", message);
        // Do what we will with the messages here.
        
        let mut message_string: String = message.to_string();
        let mut split = message_string.split(":");
        let vec_split = split.collect::<Vec<&str>>();
        if vec_split.len() > 2 {
            println!("The message: {}", vec_split[2]);
            let user_message = vec_split[2];
            
            let user_message_split = 
                user_message.split(" ").collect::<Vec<&str>>();
            
            
            if user_message_split[0] == "!rand" {
                if user_message_split.len() > 1 {
                    let mut user_num = user_message_split[1].trim()
                        .parse::<i32>();
                    match user_num {
                        Ok(val) => { 
                            println!("It's a number. {:?}", val);
                            let mut user_num_int = &user_num.unwrap();
                            let mut rng = rand::thread_rng();
                            if user_num_int > &1 && user_num_int < &2147483647 {
                                //Thanks Solid_ for showing me this will crash...
                                //Twice
                                
                                let rand_response = rng.gen_range
                                    (1, user_num_int + 1);
                                let response_string = 
                                    format!("Your number is: {:?}", 
                                        rand_response);
                            
                                client.send_privmsg
                                    ("#xonotic.butt", response_string).unwrap();
                            }
                            
                        },
                        Err(why) => println!("It's not a number. {}", why),
                    }
                }
            }
        }
        
        Ok(())
    });
    
    reactor.run().unwrap();
}
